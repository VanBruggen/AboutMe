# About me

Information additional to the one found in my resume, as not every relevant detail would fit on a single A4 sheet.

## Table of contents

1. [🧁 For whoever is reading this](#for-whoever-is-reading-this)
1. [Traits](#traits)
1. [🎯 IT interests](#it-interests)
    - [Cybersec](#cybersec-equal-dose-of-theory-and-practice)
    - [Administration](#administration-mostly-theory)
    - [Programming](#programming-mostly-theory)
1. [💡 Examples of rapid learning](#examples-of-rapid-learning)
1. [🔧 Examples of my passion for cybersecurity and administration](#examples-of-my-passion-for-cybersecurity-and-administration)
1. [▶️ Favorite IT YouTube channels](#favorite-it-youtube-channels)
    - [Cybersec & Privacy](#cybersec-privacy)
    - [Programming & DevOps](#programming-devops)
    - [Administration](#administration)
    - [Everything above](#everything-above)
1. [🎧️ Favorite IT podcasts](#favorite-it-podcasts)
    - [Everything](#everything)
    - [Programming & DevOps](#programming-devops-1)
    - [Administration](#administration-1)
    - [Cybersec & Privacy](#cybersec-privacy-1)

## For whoever is reading this

To make your day and outlook on life a little better -- did you know that in order to become quite good at something, you don't need the (in)famous 10,000 hours, but only 20? Don't believe me? Then be sure to check out [this brilliant 3-minute TED talk](https://www.youtube.com/watch?v=isrgSDjrprg) ;)


## Traits

- 🗨️ Clear communicator and explanaitor.
- 💡 Rapid learner.
- 🔬 Easily enters and sustains the "flow state".
- :arrow_down: "From the general to the specific" approach.
- 🚩 Goal-oriented, no BS type.
- 🗂️ Neatly organized & neat organizer.
- 📙 Definitely the one, who reads manuals before using newly bought devices.

## IT interests

#### Cybersec (equal dose of theory and practice)

- 🐃🐧 FOSS software & hardware
- 🛡️ Digital privacy
- 🛠️ Digital self-reliance
- 🔒️ Encryption, authentication

#### Administration (mostly theory)
- 📡 Computer networks
- 🔧 Systems administration
- 🌐 Hosting

#### Programming (equal dose of theory and practice)
- 💡 Software engineering:

    - Gojko Adzic's 'Specification by example'
    - Modelling with UML and/or C4
    - BDD -> TDD
    - SOLID, DRY, KISS (the most important SDK every dev needs)
    - Design Patterns

- 👨‍💻 Full stack webdev:

    - Practice:
        - Java
        - SQL
        - MongoDB
        - JSON, XML
        - Python

    - A touch of practice:
        - HTML (semantic elements, not a sea of `<div>`'s and `<span>`'s)
        - CSS (separate files; semantic elements > classes > IDs, inline styling stinks)
        
    - Theory:
        - REST API
        - HTTP protocol
        - Single Page Applications
        - Server Side Rendering
        - Headless CMS
        - Microservices
        - Web server and its role
        - Django

    - A touch of theory:
        - JS and its frameworks (from the syntax perspective, Svelte appeals to me the most)
        - NodeJS
        - GraphQL
        - MIME types
        - Graph databases
        - PHP
        - Wordpress

- 🟦 Mobile and desktop apps with Flutter:
    
    - Made 3/4 of an app working on Android, Windows and Linux. See the "Septimus" repo.



# Examples of rapid learning

- Learned most of [Dart](https://dart.dev) and 1/3 of [Flutter](https://flutter.dev) in three weeks.
- Learned [AsciiDoc](https://asciidoc.org/) and a big chunk of [PlantUML](https://plantuml.com/) in a week.
- Learned most of :green_circle: [QtWidgets](https://doc.qt.io/qt-5/qtwidgets-index.html) and proper UI layout theory in five days.
- Learned how to setup a [LUKS + LVM](https://en.wikipedia.org/wiki/Linux_Unified_Key_Setup#Using_LUKS_with_LVM) partition through the terminal in three days.
- Learned 🐱 Git in 1.5 days.



# Examples of my passion for cybersecurity and administration

- [KeePassXC](https://keepassxc.org/) 🔒️ is my essential, everyday tool.

- I know [Diceware](https://en.wikipedia.org/wiki/Diceware) 🎲 by heart, own an improved wordlist and propagate using this method wherever I can (not as well as [XKCD](https://xkcd.com/936/) of course, but their adviced passphrase length is long outdated). Succesfuly taught it to my 9 y.o. sister.
- My daily driver system is installed on a LUKS-encrypted, 7-word Diceware secured, LVM-organized partition.
- I have a [Back In Time](https://backintime.readthedocs.io/en/latest/index.html#introduction) 🕑️ backup of my whole system set up to an external, LUKS-encrypted, 7-word Diceware secured, 256 GB USB 3.2 flash drive.
- I created a disk image 💽 of my whole Linux system and cloned it to my old laptop, all within 2 days.
- In order not to manually check for updates for touchpad gestures software on github, I learned and set up an [anacron](https://www.tutorialspoint.com/unix_commands/anacron.htm) ⏳️ job in like, 3 hours. It works like a charm ever since, checking for new release every 21 days and installing it if one is present.
- Established an own, localhost server of [Kroki](https://kroki.io/) using Docker :whale:, because I wanted to familiarize myself with this tech and also wanted more privacy and self-reliance.
- I use ExpressVPN 🛡️ all the time and control it through the terminal.
- My daily browser is a thoroughly hardened Firefox 🦊 -- a [custom user.js](https://github.com/arkenfox/user.js/), [uBlock Origin](https://ublockorigin.com/) 🔴 (I'm an advanced user, who can use dynamic rules), [Decentraleyes](https://decentraleyes.org/) 👁️, [ClearURLs](https://github.com/ClearURLs/Addon) 🧹 and [Universal Bypass](https://universal-bypass.org/) ➡️.
- I use a mail client for maximum privacy & security.
- My e-mail provider is the safest generally-available one on the planet, with unlimited, disparate aliases.
- I'm aware of device fingerprinting 📱 (basic via HTTP user-agent and advanced via JS).
- I'm well aware of post-quantum cryptography and the [NTRU](https://en.wikipedia.org/wiki/NTRU) algorithm (the most promising one to replace RSA and Curve25519). I even had plans to create a quantum-proof messenger based upon it, read a [Medium article](https://medium.com/autobahnnetwork/the-essence-of-ntru-key-generation-encryption-decryption-7c0540ef8441) diving deep into the algorithm's details and were perfectly ready to implement it :)



# Favorite IT YouTube channels

#### Cybersec & Privacy
- ❤️ [MentalOutlaw](https://www.youtube.com/c/MentalOutlaw)
- [Computerphile](https://www.youtube.com/user/Computerphile)

#### Programming & DevOps
- ❤️ [overment](https://www.youtube.com/c/overment)
- ❤️ [Fireship](https://www.youtube.com/c/Fireship)



# Favorite IT podcasts

#### Everything
- ❤️ [Porozmawiajmy o IT](https://porozmawiajmyoit.pl/)
- [IT Kwadrans](https://itkwadrans.pl/)

#### Programming & DevOps
- ❤️ [DevEnv](https://devenv.pl/podcast/)

#### Administration
- [Chwila dla admina](https://chwiladlaadmina.pl/podcast/)

#### Cybersec & Privacy
- ❤️ [Na Podsłuchu](https://niebezpiecznik.pl/podcast/)
- [NajslabszeOgniwo.pl](https://open.spotify.com/show/2tDkm7y288ETVsO1V8CKM2)
- [bezpieczny podcast](https://bezpieczny.blog/podcast/)